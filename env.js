const env = {
  username: process.env.AVANSUSER,
  password: process.env.AVANSPASS
}

module.exports = env;