const puppeteer = require('puppeteer');
const env = require('./env');

class RoosterService {

  constructor() {

  }

  async getRoosterPage() {
    const browser = await puppeteer.launch({
      args: ['--no-sandbox', '--disable-setuid-sandbox'],
      headless: true
    });
    
    const page = await browser.newPage();
    await page.goto('https://rooster.avans.nl');
    await page.click('.idp-list > .result');

    await page.waitForNavigation({
      waitUntil: 'load'
    });
    await page.waitFor(1000);
    await page.keyboard.type(env.username);
    await page.click('#Ecom_Password');
    await page.keyboard.type(env.password);
    await page.click('#loginButton2');

    await page.waitForNavigation({
      waitUntil: 'load'
    });
    await page.waitForNavigation({
      waitUntil: 'load'
    });
    await page.waitForNavigation({
      waitUntil: 'load'
    });
    await page.waitFor(250);

    return page;
  }

  async getRoosterFromGroup(group) {
    const page = await this.getRoosterPage();
    await page.waitFor(500);
    await page.click('#auto-G');
    await page.keyboard.type(group);
    await page.waitFor(1000);

    try {
      await page.click('.ui-autocomplete > .ui-menu-item > a');
    } catch (error) {
      return [];
    }

    await page.waitFor(1000);

    const thisWeekEvents = {
      monday: await this.getEventsFromDay(page, 'day-1'),
      tuesday: await this.getEventsFromDay(page, 'day-2'),
      wednesday: await this.getEventsFromDay(page, 'day-3'),
      thursday: await this.getEventsFromDay(page, 'day-4'),
      friday: await this.getEventsFromDay(page, 'day-5')
    }

    await page.click('.wc-prev');
    await page.waitFor(1000);

    const prevWeekEvents = {
      monday: await this.getEventsFromDay(page, 'day-1'),
      tuesday: await this.getEventsFromDay(page, 'day-2'),
      wednesday: await this.getEventsFromDay(page, 'day-3'),
      thursday: await this.getEventsFromDay(page, 'day-4'),
      friday: await this.getEventsFromDay(page, 'day-5')
    }

    await page.click('.wc-next');
    await page.click('.wc-next');
    await page.waitFor(1000);

    const nextWeekEvents = {
      monday: await this.getEventsFromDay(page, 'day-1'),
      tuesday: await this.getEventsFromDay(page, 'day-2'),
      wednesday: await this.getEventsFromDay(page, 'day-3'),
      thursday: await this.getEventsFromDay(page, 'day-4'),
      friday: await this.getEventsFromDay(page, 'day-5')
    }

    const events = {
      prevWeek: prevWeekEvents,
      thisWeek: thisWeekEvents,
      nextWeek: nextWeekEvents
    }

    return events;
  }

  async getEventsFromDay(page, day) {
    return await page.evaluate((sel) => {
      const roosterEvents = [];
      const events = document.querySelectorAll(sel);
      for (const event in events) {
        if (events.hasOwnProperty(event)) {
          const element = events[event];
          const text = element.innerText;
          splittedEvent = text.split('\n');
          teacherWithLocation = splittedEvent[2].split(' in ');
          roosterEvents.push({
            time: splittedEvent[0].substring(5),
            name: splittedEvent[1],
            teacher: teacherWithLocation[0],
            location: teacherWithLocation[1]
          });
        }
      }
      return roosterEvents;
    }, `.${day} > .wc-full-height-column > .wc-cal-event`);
  }
}

module.exports = RoosterService;
