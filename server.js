'use strict';
const express = require('express');
const app = express();
const path = require('path');
const http = require('http');

const RoosterService = require('./rooster-service');
const roosterService = new RoosterService();

app.use(express.static(path.join(__dirname, 'public')))

app.get('/', (req, res) => {
  res.writeHead(200, {
    'Content-Type': 'text/html'
  });
  res.write('<h1>Welcome to the Avans Rooster API</h1>');
  res.end();
});

app.get('/test', (req, res) => {
  res.json({
    test: 123
  });
});

app.get('/api/v1/rooster', async (req, res) => {
  const group = req.query.group;
  try {
    const rooster = await roosterService.getRoosterFromGroup(group);
    res.json(
      rooster
    );
  } catch (error) {
    console.log(error);
    res.json({
      error: "Could not find rooster"
    });
  }
});

const port = process.env.PORT || 3000;
const server = http.createServer(app);
server.listen(port, () => {
  console.log(`server running on port ${port}`)
});
